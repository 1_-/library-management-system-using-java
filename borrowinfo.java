import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class borrowinfo extends JFrame implements ActionListener
{
	JLabel userLabel, eNameLabel, phoneLabel, roleLabel,welcomeLabel,salaryLabel,finelabel;
	JTextField userTF, phoneTF1, phoneTF2, eNameTF, roleTF, salaryTF,finetf;
	JButton refreshBtn, loadBtn, updateBtn, delBtn, backBtn, logoutBtn,backnext;
	JPanel panel;
	
	String userId;
	String id;
	String i;
	String eName2 = null;
	
	public borrowinfo(String userId)
	{
		super("borrow info");
		
		this.setSize(800, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.userId = userId;
		
		panel = new JPanel();
		panel.setLayout(null);
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		eNameLabel = new JLabel("BorrowId Id : ");
		eNameLabel.setBounds(250, 100, 120, 30);
		panel.add(eNameLabel);
		
		eNameTF = new JTextField();
		eNameTF.setBounds(400, 100, 120, 30);
		panel.add(eNameTF);
		
		phoneLabel = new JLabel("book ID : ");
		phoneLabel.setBounds(250, 150, 120, 30);
		panel.add(phoneLabel);
		
		phoneTF2 = new JTextField();
		phoneTF2.setBounds(400, 150, 120, 30);
		panel.add(phoneTF2);
		
		roleLabel = new JLabel("Borrow Date : ");
		roleLabel.setBounds(250, 200, 120, 30);
		panel.add(roleLabel);
		
		roleTF = new JTextField();
		roleTF.setBounds(400, 200, 120, 30);
		panel.add(roleTF);
		
		salaryLabel = new JLabel("return Date : ");
		salaryLabel.setBounds(250, 250, 120, 30);
		panel.add(salaryLabel);
		
		salaryTF = new JTextField();
		salaryTF.setBounds(400, 250, 120, 30);
		panel.add(salaryTF);
		
		finelabel = new JLabel("Fine : ");
		finelabel.setBounds(250, 300, 120, 30);
		panel.add(finelabel);
		
		finetf = new JTextField();
		finetf.setBounds(400, 300, 120, 30);
		panel.add(finetf);
		
		backBtn = new JButton("Back");
		backBtn.setBounds(350, 400, 120, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		backnext = new JButton("Next-->");
		backnext.setBounds(500, 400, 120, 30 );
		backnext.addActionListener(this);
		panel.add(backnext);
		
		this.add(panel);
		
		String loadId = userId;
		String query = "SELECT `borrowId`, `bookId` ,`borrowDate` ,`returnDate`,`fine` FROM `borrowinfo` WHERE `userId`='"+loadId+"';";    
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String eName = null;
			String phnNo = null;
			String role = null;
			String returnDate = null;
			String fine = null;		
			while(rs.next())
			{
				eNameTF.setText("");
				phoneTF2.setText("");
				roleTF.setText("");
				salaryTF.setText("");
				finetf.setText("");
				
                eName = rs.getString("borrowId");
				phnNo = rs.getString("bookId");
				role = rs.getString("borrowDate");
				returnDate = rs.getString("returnDate");
				fine = rs.getString("fine");
				flag=true;
				
				eNameTF.setText(eName);
				id = eNameTF.getText();
				phoneTF2.setText(phnNo);
				roleTF.setText(role);
				salaryTF.setText(returnDate);
				finetf.setText(fine);
				eNameTF.setEnabled(false);
				phoneTF2.setEnabled(false);
				roleTF.setEnabled(false);
				salaryTF.setEnabled(false);
				finetf.setEnabled(false);
				
			}
			if(!flag)
			{
				userTF.setText(loadId);
				eNameTF.setText("");
				phoneTF2.setText("");
				roleTF.setText("");
				salaryTF.setText("");
				finetf.setText("");
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }	
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			CustomerHome me = new CustomerHome(userId);
			me.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(backnext.getText())) 
		{
			next();
		}
		else{}
	}	
	void next()
	{
		String loadId = userId;
		
		String query = "SELECT `borrowId`, `bookId` ,`borrowDate` ,`returnDate`,`fine` FROM `borrowinfo` WHERE `userId`='"+loadId+"' AND NOT `borrowId` ='"+id+"' AND NOT `borrowId` ='"+i+"';";    
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			
			String phnNo2 = null;
			String role2 = null;
			String returnDate2 = null;
			String fine2 = null;			
			while(rs.next())
			{
                eName2 = rs.getString("borrowId");
				phnNo2 = rs.getString("bookId");
				role2 = rs.getString("borrowDate");
				returnDate2 = rs.getString("returnDate");
				fine2 = rs.getString("fine");
				flag=true;
				
				
				eNameTF.setText(eName2);
				phoneTF2.setText(phnNo2);
				roleTF.setText(role2);
				salaryTF.setText(returnDate2);
				finetf.setText(fine2);
				eNameTF.setEnabled(false);
				phoneTF2.setEnabled(false);
				roleTF.setEnabled(false);
				salaryTF.setEnabled(false);
				finetf.setEnabled(false);
				
			}
			
			if(!flag)
			{
				userTF.setText(loadId);
				eNameTF.setText("");
				phoneTF2.setText("");
				roleTF.setText("");
				salaryTF.setText("");
				finetf.setText("");
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
			id = i;
			i = eNameTF.getText();		
	}
}