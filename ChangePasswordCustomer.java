import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class ChangePasswordCustomer extends JFrame implements ActionListener
{
	JLabel oldPassLabel, newPassLabel;
	JTextField oldPassTF, newPassTF;
	JButton changeBtn, backBtn, logoutBtn;
	JPanel panel;
	String userId;
	
	public ChangePasswordCustomer(String userId)
	{
		super("Library Management System - Change Password Window");
		
		this.userId = userId;
		this.setSize(800,450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		oldPassLabel = new JLabel("Old Password : ");
		oldPassLabel.setBounds(270, 100, 160, 30);
		panel.add(oldPassLabel);
		
		oldPassTF = new JTextField();
		oldPassTF.setBounds(370, 100, 100, 30);
		panel.add(oldPassTF);
		
		newPassLabel = new JLabel("New Password : ");
		newPassLabel.setBounds(270, 150, 170, 30);
		panel.add(newPassLabel);
		
		newPassTF = new JPasswordField();
		newPassTF.setBounds(370, 150, 100, 30);
		panel.add(newPassTF);
		
		
		changeBtn = new JButton("Change");
		changeBtn.setBounds(300, 200, 80, 30);
		changeBtn.addActionListener(this);
		panel.add(changeBtn);
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(390, 200, 80, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		this.add(panel);
	}
	public void actionPerformed(ActionEvent ae)
	{   String str = ae.getActionCommand();
		if(str.equals(changeBtn.getText()))
		{
			changepass();
		}
		else if(str.equals(backBtn.getText()))
		{
			CustomerHome ch = new CustomerHome(userId);
			ch.setVisible(true);
			this.setVisible(false);
		}
		else if(str.equals(logoutBtn.getText()))
		{
			Login l = new Login();
			l.setVisible(true);
			this.setVisible(false);
		}
		
	}
	public void changepass()
	{
		String loadId = userId;
		String query = "SELECT `userId`, `password`, `status` FROM `login` WHERE `userId`='"+loadId+"';";    
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			
			System.out.println("ok");
			boolean flag = false;
			System.out.println("ok");
						
			while(rs.next())
			{
				String password = rs.getString("password");
				String s = newPassTF.getText();
				if( password.equals(oldPassTF.getText()))
				{
					flag=true;
					String query1 = "UPDATE login SET password = '"+s+"' WHERE userId='"+userId+"'";	
					Connection con1=null;//for connection
					Statement st1 = null;//for query execution
					System.out.println(query1);
				try
				{
					Class.forName("com.mysql.jdbc.Driver");//load driver
					con1 = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
					st1 = con.createStatement();//create statement
					st1.executeUpdate(query1);
					st1.close();
					con1.close();
					JOptionPane.showMessageDialog(this, "Success !!!");
					Login l = new Login();
					l.setVisible(true);
					this.setVisible(false);
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					JOptionPane.showMessageDialog(this, "Oops !!!");
				}
					
				}
				else{}
			
		}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid  Password"); 
			}		
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}
}