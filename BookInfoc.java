import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class BookInfoc extends JFrame implements ActionListener
{
	JLabel userLabel, eNameLabel, phoneLabel, roleLabel, salaryLabel;
	JTextField userTF, phoneTF1, phoneTF2, eNameTF, roleTF, salaryTF;
	JButton refreshBtn, loadBtn, updateBtn, delBtn, backBtn, logoutBtn;
	JPanel panel;
	
	String userId;
	
	public BookInfoc(String userId)
	{
		super("BookInfo");
		
		this.setSize(800, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.userId = userId;
		
		panel = new JPanel();
		panel.setLayout(null);
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		refreshBtn = new JButton("Refresh");
		refreshBtn.setBounds(250, 100, 275, 30);
		refreshBtn.addActionListener(this);
		panel.add(refreshBtn);
		
		userLabel = new JLabel("BOOK ID : ");
		userLabel.setBounds(250, 200, 120, 30);
		panel.add(userLabel);
		
		userTF = new JTextField();
		userTF.setBounds(400, 200, 120, 30);
		panel.add(userTF);
		
		loadBtn = new JButton("Load");
		loadBtn.setBounds(250, 400, 120, 30);
		loadBtn.addActionListener(this);
		panel.add(loadBtn);
		
		eNameLabel = new JLabel("BOOK Name : ");
		eNameLabel.setBounds(250, 150, 120, 30);
		panel.add(eNameLabel);
		
		eNameTF = new JTextField();
		eNameTF.setBounds(400, 150, 120, 30);
		panel.add(eNameTF);
		
		phoneLabel = new JLabel("Author Name : ");
		phoneLabel.setBounds(250, 250, 120, 30);
		panel.add(phoneLabel);
		
		phoneTF2 = new JTextField();
		phoneTF2.setBounds(400, 250, 120, 30);
		panel.add(phoneTF2);
		
		roleLabel = new JLabel("Publication Year ");
		roleLabel.setBounds(250, 300, 120, 30);
		panel.add(roleLabel);
		
		roleTF = new JTextField();
		roleTF.setBounds(400, 300, 120, 30);
		panel.add(roleTF);
		
		salaryLabel = new JLabel("Available Quantity: ");
		salaryLabel.setBounds(250, 350, 120, 30);
		panel.add(salaryLabel);
		
		salaryTF = new JTextField();
		salaryTF.setBounds(400, 350, 120, 30);
		panel.add(salaryTF);
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 400, 120, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			CustomerHome me = new CustomerHome(userId);
			me.setVisible(true);
			this.setVisible(false);
		}
		if(text.equals(refreshBtn.getText()))
		{
			/*updateBtn.setEnabled(false);
			delBtn.setEnabled(false);*/
			userTF.setEnabled(true);
			userTF.setText("");
			eNameTF.setText("");
			//phoneTF1.setText("");
			phoneTF2.setText("");
			roleTF.setText("");
			salaryTF.setText("");
		}
		else if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(loadBtn.getText()))
		{
			loadFromDB();			
		}
		
		else{}
	}
	
	public void loadFromDB()
	{
		String loadId = eNameTF.getText();
		String query = "SELECT `bookId`, `bookTitle`, `authorName`, `publicationYear`, `availableQuantity`  FROM `book` WHERE `bookTitle`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;
			String id = null;
			String eName = null;
			String phnNo = null;
			int role = 0;
			double salary = 0.0;
			int av = 0;
			while(rs.next())
			{
				id = rs.getString("bookId");
                eName = rs.getString("bookTitle");
				phnNo = rs.getString("authorName");
				role = rs.getInt("publicationYear");
				salary = rs.getInt("availableQuantity");
				
				flag=true;
				userTF.setText(id);
				eNameTF.setText(eName);
				phoneTF2.setText(phnNo);
				roleTF.setText(""+role);
				salaryTF.setText(""+salary);
				userTF.setEnabled(false);
				/*updateBtn.setEnabled(true);
				delBtn.setEnabled(true);*/
			}
			if(!flag)
			{
				eNameTF.setText("");
				phoneTF1.setText("");
				phoneTF2.setText("");
				roleTF.setText("");
				salaryTF.setText("");
				JOptionPane.showMessageDialog(this,"Invalid ID"); 
			}
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this,"Book Not Available");
			this.setVisible(false);
			this.setVisible(true);
			
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex)
			{}
        }
	}
}