import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class CustomerHome extends JFrame implements ActionListener
{
	JLabel welcomeLabel;
	JButton changePasswordBtn, logoutBtn,viewDetailsBtn,borrowinfo,booksc;
	JPanel panel;
	String userId;
	
	public CustomerHome(String userId)
	{
		super("Library Management System - Customer Home Window");
		
		this.userId = userId;
		this.setSize(800,450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		String loadId = userId;
		String query = "SELECT `customerName` FROM `customer` WHERE `userId`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			st = con.createStatement();//create statement
			rs = st.executeQuery(query);//getting result
			
			String cName = null;
			while(rs.next())
			{
                cName = rs.getString("customerName");
				welcomeLabel = new JLabel("Welcome, "+cName);
				welcomeLabel.setBounds(350, 50, 100, 30);
				panel.add(welcomeLabel);
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
		
		logoutBtn = new JButton("logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		viewDetailsBtn = new JButton("My Information");
		viewDetailsBtn.setBounds(400, 120, 150, 30);
		viewDetailsBtn.addActionListener(this);
		panel.add(viewDetailsBtn);
		
		
		borrowinfo = new JButton("borrowinfo");
		borrowinfo.setBounds(220, 120, 150, 30);
		borrowinfo.addActionListener(this);
		panel.add(borrowinfo);
		
		booksc = new JButton("Book Info");
		booksc.setBounds(400, 195, 150, 30);
		booksc.addActionListener(this);
		panel.add(booksc);
		
		changePasswordBtn = new JButton("Change Password");
		changePasswordBtn.setBounds(220, 195, 150, 30);
		changePasswordBtn.addActionListener(this);
		panel.add(changePasswordBtn);
		
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(viewDetailsBtn.getText()))
		{
			CustomerDetails cp = new CustomerDetails(userId);
			cp.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(borrowinfo.getText()))
		{
			borrowinfo cp = new borrowinfo(userId);
			cp.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(booksc.getText()))
		{
			BookInfoc cp = new BookInfoc(userId);
			cp.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(changePasswordBtn.getText()))
		{
			ChangePasswordCustomer cp = new ChangePasswordCustomer(userId);
			cp.setVisible(true);
			this.setVisible(false);
		}
	}
}