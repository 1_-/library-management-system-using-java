import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.lang.Object;
public class Borrowbook extends JFrame implements ActionListener
{
		JLabel userLabel, eNameLabel, phoneLabel, roleLabel, salaryLabel,customerLabel;
	JTextField userTF, phoneTF1, phoneTF2, eNameTF, roleTF, salaryTF,customerTf;
	JButton refreshBtn, loadBtn, updateBtn, delBtn, backBtn, logoutBtn,borrowbtn;
	JPanel panel;
	
	String userId;
	
	public Borrowbook(String userId)
	{
		super("BookInfo");
		
		this.setSize(800, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.userId = userId;
		
		panel = new JPanel();
		panel.setLayout(null);
		
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		refreshBtn = new JButton("Refresh");
		refreshBtn.setBounds(250, 50, 275, 30);
		refreshBtn.addActionListener(this);
		panel.add(refreshBtn);
		
		customerLabel = new JLabel("Customer Id : ");
		customerLabel.setBounds(250, 100, 120, 30);
		panel.add(customerLabel);
		
		customerTf= new JTextField();
		customerTf.setBounds(400, 100, 120, 30);
		panel.add(customerTf);
		
		userLabel = new JLabel("BOOK ID : ");
		userLabel.setBounds(250, 150, 120, 30);
		panel.add(userLabel);
		
		userTF = new JTextField();
		userTF.setBounds(400, 150, 120, 30);
		panel.add(userTF);
		
		eNameLabel = new JLabel("Borrow Id  : ");
		eNameLabel.setBounds(250, 200, 120, 30);
		panel.add(eNameLabel);
		
		Random r = new Random();
		
		eNameTF = new JTextField(""+r.nextInt(9999));
		eNameTF.setBounds(400, 200, 120, 30);
		eNameTF.setEnabled(false);
		panel.add(eNameTF);
		
		//DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		System.out.println(""+date.getDate());
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now)); //2016/11/16 12:08:43
		
		phoneLabel = new JLabel("Borrow Date : ");
		phoneLabel.setBounds(250, 250, 120, 30);
		panel.add(phoneLabel);
		
		phoneTF2 = new JTextField(dtf.format(now));
		phoneTF2.setBounds(400, 250, 120, 30);
		panel.add(phoneTF2);
		
		roleLabel = new JLabel("Return Date : ");
		roleLabel.setBounds(250, 300, 120, 30);
		panel.add(roleLabel);
		
		int u = Integer.parseInt(dtf.format(now).substring(8,10));
		
		u = u + 7;
		
		String es = dtf.format(now).substring(0,8)+Integer.toString(u);
		
		roleTF = new JTextField(es);
		roleTF.setBounds(400, 300, 120, 30);
		panel.add(roleTF);
		
		/*salaryLabel = new JLabel("Available Quantity: ");
		salaryLabel.setBounds(250, 350, 120, 30);
		panel.add(salaryLabel);
		
		salaryTF = new JTextField();
		salaryTF.setBounds(400, 350, 120, 30);
		panel.add(salaryTF);*/
		
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 400, 120, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		borrowbtn = new JButton("borrow");
		borrowbtn.setBounds(375, 400, 120, 30);
		borrowbtn.addActionListener(this);
		panel.add(borrowbtn);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			EmployeeHome me = new EmployeeHome(userId);
			me.setVisible(true);
			this.setVisible(false);
		}
		if(text.equals(refreshBtn.getText()))
		{
			/*updateBtn.setEnabled(false);
			delBtn.setEnabled(false);*/
			userTF.setEnabled(true);
			userTF.setText("");
			Random r1 = new Random();
			eNameTF.setText(""+r1.nextInt(9999));
			//phoneTF1.setText("");
			phoneTF2.setText("");
			roleTF.setText("");
			salaryTF.setText("");
		}
		else if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		/*else if(text.equals(loadBtn.getText()))
		{
			loadFromDB();			
		}*/
		else if(text.equals(borrowbtn.getText()))
		{
			loadFromDB();
		}
		else{}
	}
	
	public void loadFromDB()
	{
		String borrowid = eNameTF.getText();
		String bookid = userTF.getText();
		String customerid = customerTf.getText();
		String borrowdate = phoneTF2.getText();
		String returndate = roleTF.getText();
		String fine = "0";
		
		
		String query1 = "INSERT INTO borrowinfo VALUES ('"+borrowid+"','"+bookid+"','"+customerid+"','"+borrowdate+"','"+returndate+"','"+fine+"');";
		String query2 ="SELECT `availableQuantity` FROM `book`WHERE `bookId`='"+bookid+"';"; 
		
		System.out.println(query1);
		System.out.println(query2);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("ok");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22", "root", "");
			System.out.println("ok");
			Statement stm = con.createStatement();
			System.out.println("ok");
			//stm.execute(query1);
			System.out.println("ok");
			//rs=st.execute(query2);
			ResultSet rs = null;
		    Statement st = null;
			st = con.createStatement();
			rs=st.executeQuery(query2);
			System.out.println("ok");
			while(rs.next())
			{
			int i = rs.getInt("availableQuantity");
			 if(i != 0)
			 {
				 stm.execute(query1);
					i = i-1;
			String query3 = "UPDATE book SET availableQuantity='"+i+"' WHERE bookId='"+bookid+"'"; 
			stm.execute(query3);
			System.out.println("ok");
			JOptionPane.showMessageDialog(this, "Success !!!");
			 }
			 else
			 {
				 JOptionPane.showMessageDialog(this, "Book Unavailable");
			 }
			}
			stm.close();
			con.close();
			st.close();
			rs.close();
			
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this, "Oops !!!..invalid id");
        }
	}
}
