import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class EmployeeHome extends JFrame implements ActionListener
{
	JLabel welcomeLabel;
	JButton manageEmployeeBtn, changePasswordBtn, viewDetailsBtn, logoutBtn, bookDetails, borrowBtn,returnBtn,customerBtn,addbookBtn;
	JPanel panel;
	String userId;
	
	public EmployeeHome(String userId)
	{
		super("Library Management System - Employee Home Window");
		
		this.userId = userId;
		this.setSize(800,450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		String loadId = userId;
		String query = "SELECT `name` FROM `employee` WHERE `userId`='"+loadId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			String cName = null;
			while(rs.next())
			{
                cName = rs.getString("name");
				welcomeLabel = new JLabel("Welcome, "+cName);
				welcomeLabel.setBounds(350, 50, 100, 30);
				panel.add(welcomeLabel);
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		changePasswordBtn = new JButton("Change Password");
		changePasswordBtn.setBounds(220, 260, 150, 30);
		changePasswordBtn.addActionListener(this);
		panel.add(changePasswordBtn);
		
		manageEmployeeBtn = new JButton("Manage Employee");
		manageEmployeeBtn.setBounds(220, 120, 150, 30);
		manageEmployeeBtn.addActionListener(this);
		panel.add(manageEmployeeBtn);
		
		viewDetailsBtn = new JButton("My Information");
		viewDetailsBtn.setBounds(400, 120, 150, 30);
		viewDetailsBtn.addActionListener(this);
		panel.add(viewDetailsBtn);
		
		bookDetails = new JButton("Book Info");
		bookDetails.setBounds(220, 330, 150, 30);
		bookDetails.addActionListener(this);
		panel.add(bookDetails);
		
		borrowBtn = new JButton("Borrow");
		borrowBtn.setBounds(400, 190, 150, 30);
		borrowBtn.addActionListener(this);
		panel.add(borrowBtn);
		
		returnBtn = new JButton("return");
		returnBtn.setBounds(220, 190, 150, 30);
		returnBtn.addActionListener(this);
		panel.add(returnBtn);
		
		customerBtn = new JButton("customer info");
		customerBtn.setBounds(400, 260, 150, 30);
		customerBtn.addActionListener(this);
		panel.add(customerBtn);
		
		addbookBtn = new JButton("Add Book");
		addbookBtn.setBounds(400, 330, 150, 30);
		addbookBtn.addActionListener(this);
		panel.add(addbookBtn);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(changePasswordBtn.getText()))
		{
			ChangePassword cp = new ChangePassword(userId);
			cp.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(manageEmployeeBtn.getText()))
		{
			//String loadId = userId;
		String query = "SELECT `role` FROM `employee` WHERE `userId`='"+userId+"';";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			String role = null;
			while(rs.next())
			{
                role = rs.getString("role");
				if(role.equals("Maneger"))
				{
					ManageEmployee me = new ManageEmployee(userId);
				me.setVisible(true);
				this.setVisible(false);
			}
			else
			{
				JOptionPane.showMessageDialog(this, "Access Denied");
			}
				}
			
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
		}		
		
		else if(text.equals(bookDetails.getText()))
		{
			BookInfo bi = new BookInfo(userId);
			bi.setVisible(true);
			this.setVisible(false);
			
		}
		else if(text.equals(viewDetailsBtn.getText()))
		{
			ViewDetails vd = new ViewDetails(userId);
			vd.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(borrowBtn.getText()))
		{
			Borrowbook vd = new Borrowbook(userId);
			vd.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(returnBtn.getText()))
		{
			returnbook vd = new returnbook(userId);
			vd.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(customerBtn.getText()))
		{
			customerinfo vd = new customerinfo(userId);
			vd.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(addbookBtn.getText()))
		{
			Addbook vd = new Addbook(userId);
			vd.setVisible(true);
			this.setVisible(false);
		}
		else{}
	}
	
}